<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Campaign
 *
 * @ORM\Table(name="campaign", indexes={@ORM\Index(name="IDX_1F1512DD6DF610BF", columns={"campaign_type_id"}), @ORM\Index(name="IDX_1F1512DD9B6B5FBA", columns={"account_id"})})
 * @ORM\Entity
 */
class Campaign
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="campaign_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=450, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="message_end", type="text", nullable=false)
     */
    private $messageEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="time", nullable=false)
     */
    private $createdAt;

    /**
     * @var \AppBundle\Entity\CampaignType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CampaignType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaign_type_id", referencedColumnName="id")
     * })
     */
    private $campaignType;

    /**
     * @var \AppBundle\Entity\Members
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Members")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     * })
     */
    private $account;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMessageEnd(): string
    {
        return $this->messageEnd;
    }

    /**
     * @param string $messageEnd
     */
    public function setMessageEnd(string $messageEnd)
    {
        $this->messageEnd = $messageEnd;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return CampaignType
     */
    public function getCampaignType(): CampaignType
    {
        return $this->campaignType;
    }

    /**
     * @param CampaignType $campaignType
     */
    public function setCampaignType(CampaignType $campaignType)
    {
        $this->campaignType = $campaignType;
    }

    /**
     * @return Members
     */
    public function getAccount(): Members
    {
        return $this->account;
    }

    /**
     * @param Members $account
     */
    public function setAccount(Members $account)
    {
        $this->account = $account;
    }


}

