<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180407173219
 * Add campaign table
 */
class Version20180407173219 extends AbstractMigration
{
    public function up(Schema $schema)
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgreSQL\'.');

        $this->addSql(
            'CREATE TABLE "campaign" ( 
	                        "id" Serial NOT NULL,
	                        "account_id" Integer NOT NULL,
	                        "campaign_type_id" Integer NOT NULL,
	                        "name" Character Varying( 450 ) NOT NULL,
	                        "message_end" Text NOT NULL,
	                        "created_at" Time With Time Zone NOT NULL,
	                        PRIMARY KEY ( "id" ),
	                        CONSTRAINT "unique_campaign_id" UNIQUE( "id" ) );
        '
        );
        $this->addSql(
            'ALTER TABLE "campaign"
	                ADD CONSTRAINT "ref_campaign_campaign_type" FOREIGN KEY ( "campaign_type_id" )
	                REFERENCES "campaign_type" ( "id" ) MATCH FULL
	                ON DELETE Restrict
	                ON UPDATE Restrict;'
        );
        $this->addSql(
            'ALTER TABLE "campaign"
	                ADD CONSTRAINT "ref_campaign_members" FOREIGN KEY ( "account_id" )
	                REFERENCES "members" ( "id" ) MATCH FULL
	                ON DELETE Restrict
	                ON UPDATE Restrict;'
        );

    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgreSQL\'.');
        $this->addSql('DROP TABLE "campaign"');
    }
}
