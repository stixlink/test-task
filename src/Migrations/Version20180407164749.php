<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180407164749
 * Added members table
 */
class Version20180407164749 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgreSQL\'.');
        $this->addSql(
            'CREATE TABLE "members" ( 
        	"id" Serial NOT NULL,
        	"name" Character Varying( 250 ) NOT NULL,
        	"email" Character Varying( 2044 ) NOT NULL,
        	"phone" Character Varying( 30 ) NOT NULL,
        	"password" Character Varying( 250 ) NOT NULL,
        	"uid" Character Varying( 128 ) NOT NULL,
        	"created_at" Time With Time Zone NOT NULL,
        	PRIMARY KEY ( "id" ),
        	CONSTRAINT "unique_members_email" UNIQUE( "email" ) );
         '
        );
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgreSQL\'.');
        $this->addSql('DROP TABLE "members"');
    }
}
