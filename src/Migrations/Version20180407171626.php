<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180407171626
 * Added campaign_type table
 */
class Version20180407171626 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgreSQL\'.');
        $this->addSql(
            'CREATE TABLE "campaign_type" ( 
	                        "id" Serial NOT NULL,
	                        "name" Character Varying( 100 ) NOT NULL,
	                        "status" Character Varying( 10 ) NOT NULL,
	                        "created_at" Time With Time Zone NOT NULL,
	                        PRIMARY KEY ( "id" ),
	                        CONSTRAINT "unique_campaign_type_id" UNIQUE( "id" ) );
	                   '
        );
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgreSQL\'.');
        $this->addSql('DROP TABLE "campaign_type"');
    }
}
